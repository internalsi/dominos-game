# Dominos game

A simple domino console game developed in PHP.

#### Requirements:
 - PHP 7.0+ 
 - Composer ([installation guide](https://getcomposer.org/doc/00-intro.md))

## Steps to get it working:

1. Clone this repository
2. `cd ./dominos-game` (Get into the project directory)
3. Run `composer install`
4. Open new console window and type `play game`
5. Enjoy the game
