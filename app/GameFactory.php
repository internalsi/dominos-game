<?php

namespace App;

use App\Components\ConsoleInput;
use App\Components\ConsoleOutput;
use App\Components\ConsoleOutputColorDecorator;
use App\GameComponents\GameOutputFormatter;

class GameFactory
{
    /**
     * Creates game instance
     *
     * @return Game
     */
    public static function createGame(): Game
    {
        $game_output = new GameOutputFormatter(new ConsoleOutput(), new ConsoleOutputColorDecorator());

        return new Game(new ConsoleInput(), $game_output);
    }
}
