<?php

namespace App;

use App\Components\InputInterface;
use App\GameComponents\GameOutputFormatter;
use App\GameComponents\Player;
use App\GameComponents\PlayerFactory;
use App\GameComponents\Tile;
use App\GameComponents\TileFactory;

class Game
{
    /**
     * @var [Player] $players
     */
    private $players;

    /**
     * @var int $index
     */
    private $current;

    /**
     * @var [Tile] $tiles
     */
    private $tiles;

    /**
     * @var [Tile] $board_tiles
     */
    private $board_tiles = [];

    /**
     * @var InputInterface $input
     */
    private $input;

    /**
     * @var GameOutputFormatter $output
     */
    private $output;


    /**
     * Game constructor.
     *
     * @param InputInterface $input
     * @param GameOutputFormatter $output
     */
    public function __construct(InputInterface $input, GameOutputFormatter $output)
    {
        $this->input = $input;
        $this->output = $output;

        $this->loadPlayers();
        $this->prepareAndStart();
    }


    /**
     * Initialize game players
     *
     * @return void
     */
    private function loadPlayers(): void
    {
        $number_of_players = $this->chooseNumberOfPlayers();

        $this->players = PlayerFactory::createPlayers($number_of_players);

        $this->askForPlayerNames();

        $this->shufflePlayers();
    }

    /**
     * Choose the number of players
     *
     * @return int
     */
    private function chooseNumberOfPlayers(): int
    {
        $this->output->chooseOpponents();
        $input = (int)$this->input->read();


        if ($input > 4 || $input < 2) {
            $this->output->wrongAnswer();

            return $this->chooseNumberOfPlayers();
        }

        return $input;
    }

    /**
     * Shuffle the
     *
     * @return void
     */
    private function shuffleTiles(): void
    {
        shuffle($this->tiles);
    }

    /**
     * Assign tiles to players
     *
     * @return void
     */
    private function assignTiles(): void
    {
        foreach ($this->players as $player) {
            $player->assignTiles(array_splice($this->tiles, 0, 7));
        }
    }


    /**
     * Prepare and start the game
     *
     * @return void
     */
    public function prepareAndStart(): void
    {
        $this->resetGame();

        $this->prepareTiles();

        $this->assignTiles();

        $this->firstMove();
    }

    /**
     * Reset game
     *
     * @return void
     */
    private function resetGame(): void
    {
        $this->resetTiles();
        $this->resetCurrentPlayer();
        $this->resetBoard();
    }

    /**
     * Reset tiles
     *
     * @return void
     */
    private function resetTiles(): void
    {
        $this->tiles = [];
    }

    /**
     * Reset board
     *
     * @return void
     */
    public function resetBoard(): void
    {
        $this->board_tiles = [];
    }

    /**
     * Empty players
     *
     * @return void
     */
    public function resetPlayers(): void
    {
        $this->players = [];
    }

    /**
     * Prepare game tiles
     *
     * @return void
     */
    public function prepareTiles(): void
    {
        $this->tiles = TileFactory::generateTiles();
        $this->shuffleTiles();
    }

    /**
     * Shuffle the players
     *
     * @return void
     */
    private function shufflePlayers(): void
    {
        shuffle($this->players);
    }

    /**
     * Ask for player names
     *
     * @return void
     */
    private function askForPlayerNames(): void
    {
        foreach ($this->players as $index => $player)  {
            if (!$player->getName()) {
                $this->output->promptPlayerName($index + 1);
                $name = $this->input->read();

                if (strlen($name) < 1) {
                    $this->askForPlayerNames();
                } else {
                    $player->setName($name);
                }

            }
        }
    }

    /**
     * Reset the current player index
     *
     * @return void
     */
    private function resetCurrentPlayer(): void
    {
        $this->current = 0;
    }

    /**
     * Handle first move actions
     *
     * @return void
     */
    private function firstMove(): void
    {
        $first_player = $this->currentPlayer();
        $first_tile = $first_player->getTile();

        $this->addTileToBoardEnd($first_tile);

        $this->output->firstPlayer($first_player);
        $this->output->firstTile($first_tile);

        $this->setNextPlayer();
        $this->nextMove();
    }

    /**
     * Set index to next player in the array
     *
     * @return void
     */
    private function setNextPlayer(): void
    {
        $next = $this->current + 1;
        $this->current = !empty($this->players[$next]) ? $next : 0;
    }

    /**
     * Actions when game finished
     *
     * @return void
     */
    private function gameFinished(): void
    {
        $this->output->playerWon($this->currentPlayer());
        $this->output->playAgain();
        $this->retryGame();
    }

    /**
     * Ask if we want to retry the game
     *
     * @return void
     */
    private function retryGame(): void
    {
        $input = $this->input->read();

        if (strtoupper($input) === 'Y') {
            $this->prepareAndStart();
        }

        if (strtoupper($input) === 'N') {
            exit();
        }

        $this->output->wrongAnswer();
        $this->retryGame();
    }

    /**
     * @return void
     */
    private function nextMove(): void
    {
        $this->checkIfPlayerNeedsToDraw();
        $selected_tile = $this->playerChooseTile();

        if (!$this->addToBoardTiles($selected_tile)) {
            $this->output->invalidSelectedTile();
            $this->outputBoard();
            $this->nextMove();
        }

        $this->deductSelectedTileFromPlayer($selected_tile);

        if (count($this->currentPlayer()->tiles()) > 0) {
            $this->outputBoard();
            $this->setNextPlayer();
            $this->nextMove();
        }

        $this->gameFinished();
    }

    /**
     * Deduct selected tile from the player
     *
     * @param Tile $selected_tile
     *
     */
    private function deductSelectedTileFromPlayer(Tile $selected_tile): void
    {
        $player = $this->currentPlayer();
        $old_tiles = $player->tiles();
        $new_tiles = [];

        foreach ($old_tiles as $tile) {
            if ($selected_tile !== $tile) {
                $new_tiles[] = $tile;
            }
        }

        $player->assignTiles($new_tiles);
    }

    /**
     * Prompts player to choose a tile to connect to the tiles board
     *
     * @return Tile
     */
    private function playerChooseTile(): Tile
    {
        $player = $this->currentPlayer();
        $player_tiles = $player->tiles();

        $this->outputBoard();
        $this->output->chooseTile($player);
        $this->output->tilesList($player_tiles);

        $input = (int)$this->input->read();

        if (!$this->validInput($input)) {
            $this->output->incorrectInput();

            return $this->playerChooseTile();
        }

        return $player_tiles[$input - 1];
    }

    /**
     * Check if given player input is valid
     *
     * @param $input
     *
     * @return bool
     */
    private function validInput($input): bool
    {
        $player_tiles_count = count($this->currentPlayer()->tiles());

        return ($input > 0) && ($input <= $player_tiles_count);
    }

    /**
     * Check if player needs to draw.
     * If there are not available tiles we skip to next player
     *
     * @return void
     */
    private function checkIfPlayerNeedsToDraw(): void
    {
        if (!$this->playerHasValidTiles()) {
            $player = $this->currentPlayer();
            if ($this->tilesCount() > 0) {
                $tile = array_pop($this->tiles);
                $player->drawTile($tile);
                $this->output->drawingTile($player, $tile);
            } else {
                $this->output->noAvailableTiles($player);
                $this->setNextPlayer();
            }

            $this->nextMove();
        }
    }

    /**
     * Check if the player has any valid tiles
     *
     * @return bool
     */
    private function playerHasValidTiles(): bool
    {
        $player_tiles = $this->currentPlayer()->tiles();

        foreach ($player_tiles as $tile) {
            if ($this->validTile($tile)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the given tile can be connected to the tiles board
     *
     * @param Tile $tile
     *
     * @return bool
     */
    private function validTile(Tile $tile): bool
    {
        $leftmost_value = $this->leftmostBoardValue();
        $rightmost_value = $this->rightmostBoardValue();

        return (
            $leftmost_value === $tile->secondValue() ||
            $leftmost_value === $tile->firstValue() ||
            $rightmost_value === $tile->firstValue() ||
            $rightmost_value === $tile->secondValue()
        );
    }

    /**
     * Checks the given tile and adds it to the tiles board
     *
     * @param Tile $tile
     *
     * @return bool
     */
    private function addToBoardTiles(Tile $tile): bool
    {
        $leftmost_value = $this->leftmostBoardValue();
        $rightmost_value = $this->rightmostBoardValue();

        if ($leftmost_value === $tile->secondValue()) {
            $this->addTileToBoardBeginning($tile);

            return true;
        }

        if ($rightmost_value === $tile->firstValue()) {
            $this->addTileToBoardEnd($tile);

            return true;
        }

        $tile->flip();

        if ($leftmost_value === $tile->secondValue()) {
            $this->addTileToBoardBeginning($tile);

            return true;
        }

        if ($rightmost_value === $tile->firstValue()) {
            $this->addTileToBoardEnd($tile);

            return true;
        }

        return false;
    }

    /**
     * Add given tile to the beginning of the tiles board
     *
     * @param Tile $tile
     *
     * @return void
     */
    private function addTileToBoardBeginning(Tile $tile): void
    {
        array_unshift($this->board_tiles, $tile);
    }

    /**
     * Add given tile to the end of the tiles board
     *
     * @param Tile $tile
     *
     * @return void
     */
    private function addTileToBoardEnd(Tile $tile): void
    {
        $this->board_tiles[] = $tile;
    }

    /**
     * Get leftmost tile
     *
     * @return Tile
     */
    private function leftmostTile(): Tile
    {
        return $this->board_tiles[0];
    }

    /**
     * Get leftmost board value
     *
     * @return int
     */
    private function leftmostBoardValue(): int
    {
        return $this->leftmostTile()->firstValue();
    }

    /**
     * Get the rightmost board value
     *
     * @return int
     */
    private function rightmostBoardValue(): int
    {
        return $this->rightmostTile()->secondValue();
    }

    /**
     * Get rightmost tile
     *
     * @return Tile
     */
    private function rightmostTile(): Tile
    {
        return $this->board_tiles[count($this->board_tiles) - 1];
    }

    /**
     * Returns the current player
     *
     * @return Player
     */
    private function currentPlayer(): Player
    {
        return $this->players[$this->current];
    }

    /**
     * Returns the count of the unused tiles
     *
     * @return int
     */
    private function tilesCount(): int
    {
        return count($this->tiles);
    }

    /**
     * Output the tiles board
     *
     * @return void
     */
    private function outputBoard(): void
    {
        $this->output->formattedBoard($this->board_tiles);
    }
}
