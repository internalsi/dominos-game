<?php
namespace App;

class Application
{
    /**
     * Boot the application
     */
    public function boot(): void
    {
        GameFactory::createGame();
    }
}
