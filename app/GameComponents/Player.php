<?php

namespace App\GameComponents;

class Player
{
    /**
     * @var [Tiles] $tiles
     */
    protected $tiles = [];

    /**
     * Player name
     *
     * @var string
     */
    protected $name = null;

    /**
     * Get player name
     *
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set player name
     *
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Assign tiles to player
     *
     * @param array $tiles
     *
     * @return void
     */
    public function assignTiles(array $tiles): void
    {
        $this->tiles = array_values($tiles);
    }

    /**
     * Get first tile
     *
     * @return Tile
     */
    public function getTile(): Tile
    {
        $tile = array_pop($this->tiles);
        $this->tiles = array_values($this->tiles);

        return $tile;
    }

    /**
     * Draw tile
     *
     * @param Tile $tile
     */
    public function drawTile(Tile $tile): void
    {
        $this->tiles[] = $tile;
    }

    /**
     * Get all player tiles
     *
     * @return array
     */
    public function tiles(): array
    {
        return $this->tiles;
    }
}
