<?php

namespace App\GameComponents;

use App\Components\OutputColorDecoratorInterface;
use App\Components\OutputInterface;

class GameOutputFormatter
{
    /**
     * @var OutputInterface $output
     */
    private $output;

    /**
     * @var OutputColorDecoratorInterface $output_decorator
     */
    private $output_decorator;

    /**
     * GameOutputFormatter constructor.
     *
     * @param OutputInterface $output
     * @param OutputColorDecoratorInterface $output_decorator
     */
    public function __construct(OutputInterface $output, OutputColorDecoratorInterface $output_decorator)
    {
        $this->output = $output;
        $this->output_decorator = $output_decorator;
    }

    /**
     * Output choose opponents message
     *
     * @return void
     */
    public function chooseOpponents(): void
    {
        $this->output->write(
            $this->output_decorator->decorateRed('Choose the number of opponents [2-4]: ')
        );
    }

    /**
     * Output wrong answer message
     *
     * @return void
     */
    public function wrongAnswer(): void
    {
        $this->output->write($this->output_decorator->decorateRed('Wrong number. Try again!'));
    }

    /**
     * Enter player name message
     *
     * @param int $index
     *
     * @return void
     */
    public function promptPlayerName(int $index): void
    {
        $this->output->write($this->output_decorator->decorateBlue("Enter name for player {$index}:"));
    }

    /**
     * Output first player message
     *
     * @param Player $player
     *
     * @return void
     */
    public function firstPlayer(Player $player): void
    {
        $this->output->write(
            'Player "' . $this->output_decorator->decorateRed($player->getName()) . '" is the first to Play'
        );
    }

    /**
     * Output first tile message
     *
     * @param Tile $tile
     *
     * @return void
     */
    public function firstTile(Tile $tile): void
    {
        $text = 'Game started with first tile: '
            . $this->output_decorator->decorateYellow($this->getFormattedTile($tile));

        $this->output->write($text);
    }

    /**
     * Output play again message
     *
     * @return void
     */
    public function playAgain(): void
    {
        $this->output->write("Do you want to play again Y/N");
    }

    /**
     * Output player drawing message
     *
     * @param Player $player
     * @param Tile $tile
     */
    public function drawingTile(Player $player, Tile $tile): void
    {
        $text = "\"{$player->getName()}\" can't play, drawing tile " . $this->getFormattedTile($tile);

        $this->output->write($this->output_decorator->decorateRed($text));
    }

    /**
     * Outputs message that there are not available tiles for user to draw
     *
     * @param Player $player
     *
     * @return void
     */
    public function noAvailableTiles(Player $player): void
    {
        $text = "No available tiles for\"{$player->getName()}\" to pick. Skipping to next player";

        $this->output->write($this->output_decorator->decorateYellow($text));
    }

    /**
     * Output tiles board
     *
     * @param array $tiles
     *
     * @return void
     */
    public function formattedBoard(array $tiles): void
    {
        $text = 'Board is now: ' . $this->getFormattedBoard($tiles);

        $this->output->write($this->output_decorator->decorateYellow($text));
    }

    /**
     * Outputs choose tile message
     *
     * @param Player $player
     *
     * @return void
     */
    public function chooseTile(Player $player): void
    {
        $text = "Player \"{$player->getName()}\" turn. Please choose which tile you want to play with:";

        $this->output->write($this->output_decorator->decorateBlue($text));
    }

    /**
     * Outputs list of tiles
     *
     * @param array $tiles
     *
     * @return void
     */
    public function tilesList(array $tiles): void
    {
        foreach ($tiles as $index => $tile) {
            $text = $index + 1 . ') ' . $this->getFormattedTile($tile);

            $this->output->write($this->output_decorator->decorateYellow($text));
        }
    }

    /**
     * Outputs player won message
     *
     * @param Player $player
     *
     * @return void
     */
    public function playerWon(Player $player): void
    {
        $text = $this->output_decorator->decorateGreen(
            "Player \"{$player->getName()}\" has won !"
        );

        $this->output->write($text);
    }

    /**
     * Print incorrect input message
     *
     * @return void
     */
    public function incorrectInput(): void
    {
        $text = $this->output_decorator->decorateRed('Input incorrect. Try again.');

        $this->output->write($text);
    }


    /**
     * Print invalid tile input message
     *
     * @return void
     */
    public function invalidSelectedTile(): void
    {
        $text = $this->output_decorator->decorateRed('The selected tile is invalid.');

        $this->output->write($text);
    }

    /**
     * Format given tiles output
     *
     * @param array $tiles
     *
     * @return string
     */
    private function getFormattedBoard(array $tiles): string
    {
        return array_reduce($tiles, function ($carry, $item) {
            $output = '';

            if (!is_null($carry)) {
                $output .= $carry;
            }

            $output .= $this->getFormattedTile($item);

            return $output;
        });
    }

    /**
     * Formats given tile
     *
     * @param Tile $tile
     *
     * @return string
     */
    private function getFormattedTile(Tile $tile): string
    {
        return " <{$tile->firstValue()}:{$tile->secondValue()}> ";
    }

}
