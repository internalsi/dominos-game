<?php

namespace App\GameComponents;

class TileFactory
{
    public static function generateTiles()
    {
        $tiles = [];

        for ($i = 0; $i <= 6; $i++) {
            for ($j = $i; $j <= 6; $j++) {
                $tiles[] = new Tile($i, $j);
            }
        }

        return $tiles;
    }
}
