<?php

namespace App\GameComponents;

class PlayerFactory
{
    public static function createPlayers($number_of_players): array
    {
        $players = [];

        for ($i = 0; $i < $number_of_players; $i++) {
            $players[] = new Player();
        }

        return $players;
    }
}
