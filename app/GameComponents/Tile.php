<?php

namespace App\GameComponents;

class Tile
{
    /**
     * Tile values
     * @var array
     */
    private $values;

    /**
     * Tile constructor.
     *
     * @param int $first_value
     * @param int $second_value
     */
    public function __construct(int $first_value, int $second_value)
    {
        $this->values = [$first_value, $second_value];
    }

    /**
     * Get tile values
     *
     * @return array
     *
     * @return void
     */
    public function values(): array
    {
        return $this->values;
    }

    /**
     * Flip tile
     *
     * @return Tile
     */
    public function flip(): Tile
    {
        $this->values = array_reverse($this->values);

        return $this;
    }

    /**
     * Get the first value of the tile
     *
     * @return int
     */
    public function firstValue(): int
    {
        return $this->values[0];
    }

    /**
     * Get the second value of the tile
     *
     * @return int
     */
    public function secondValue(): int
    {
        return $this->values[1];
    }
}
