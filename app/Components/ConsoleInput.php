<?php

namespace App\Components;

class ConsoleInput implements InputInterface
{
    /**
     * Read console input
     *
     * @return string
     */
    public function read(): string
    {
        return trim(fgets(STDIN));
    }
}
