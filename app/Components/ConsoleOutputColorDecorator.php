<?php

namespace App\Components;


class ConsoleOutputColorDecorator implements OutputColorDecoratorInterface
{
    private $colors_map = [
        'BLUE' => '0;34',
        'RED' => '0;31',
        'YELLOW' => '1;33',
        'GREEN' => '0;32',
        'WHITE' => '1;37'
    ];

    /**
     * Decorate yellow
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateYellow(string $output): string
    {
        return $this->decorateStringColors($output, 'YELLOW');
    }

    /**
     * Decorate blue
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateBlue(string $output): string
    {
        return $this->decorateStringColors($output, 'BLUE');
    }

    /**
     * Decorate white
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateWhite(string $output): string
    {
        return $this->decorateStringColors($output, 'WHITE');
    }

    /**
     * Decorate red
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateRed(string $output): string
    {
        return $this->decorateStringColors($output, 'RED');
    }

    /**
     * Decorate green
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateGreen(string $output): string
    {
        return $this->decorateStringColors($output, 'GREEN');
    }


    /**
     * Add color decoration to given text
     *
     * @param string $text
     * @param string $color
     *
     * @return string
     */
    private function decorateStringColors(string $text, string $color): string
    {
        $color = $this->colors_map[strtoupper($color)];

        return "\e[{$color}m{$text}\e[0m";
    }
}
