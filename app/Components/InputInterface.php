<?php

namespace App\Components;

interface InputInterface
{
    /**
     * Read the input
     * @return string
     */
    public function read(): string;
}
