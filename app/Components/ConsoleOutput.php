<?php

namespace App\Components;

class ConsoleOutput implements OutputInterface
{
    /**
     * Write output
     * @param string $output
     */
    public function write(string $output): void
    {
        echo $output . "\n";
    }
}
