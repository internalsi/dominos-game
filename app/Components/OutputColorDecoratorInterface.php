<?php

namespace App\Components;


interface OutputColorDecoratorInterface
{
    /**
     * Decorate yellow
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateYellow(string $output): string;

    /**
     * Decorate blue
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateBlue(string $output): string;

    /**
     * Decorate white
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateWhite(string $output): string;

    /**
     * Decorate red
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateRed(string $output): string;

    /**
     * Decorate green
     *
     * @param string $output
     *
     * @return string
     */
    public function decorateGreen(string $output): string;
}
