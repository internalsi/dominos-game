<?php

namespace App\Components;

interface OutputInterface
{
    /**
     * @param string $output
     */
    public function write(string $output): void;
}
